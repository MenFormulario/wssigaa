package co.gov.mineducacion.sigaa.web.ws.business;




import javax.ejb.Local;

import co.gov.mineducacion.sigaa.web.ws.dominio.GenericResponse;
import co.gov.mineducacion.sigaa.web.ws.dominio.ReportarActoRequest;

@Local
public interface IBsNotificacionesSigaa {
	
	GenericResponse insertarActo(ReportarActoRequest acto);

}
