/**
 * 
 */
package co.gov.mineducacion.sigaa.web.ws.imp;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.apache.log4j.Logger;

import co.gov.mineducacion.sigaa.web.ws.business.IBsNotificacionesSigaa;
import co.gov.mineducacion.sigaa.web.ws.dominio.GenericResponse;
import co.gov.mineducacion.sigaa.web.ws.dominio.ReportarActoRequest;
import co.gov.mineducacion.sigaa.web.ws.interfaces.IWsNotificacionesSigaaLocal;
import co.gov.mineducacion.sigaa.web.ws.interfaces.IWsNotificacionesSigaaRemote;

/**
 * @author rlozada
 *
 */
@WebService
@Stateless(name="WsNotificacionesSigaa")
public class WsNotificacionesSigaa implements IWsNotificacionesSigaaLocal, IWsNotificacionesSigaaRemote {

	@EJB
	private IBsNotificacionesSigaa ejbNotificaciones;
	
	private static Logger logger = Logger.getLogger(WsNotificacionesSigaa.class);
	
	/* (non-Javadoc)
	 * @see co.gov.mineducacion.sigaa.web.ws.interfaces.IWsNotificacionesSigaaLocal#reportarActo(co.gov.mineducacion.sigaa.web.ws.dominio.ReportarActoRequest)
	 */
	@Override
	@WebMethod(operationName="reportarActo")
	@WebResult(name="GenericResponse")
	public GenericResponse reportarActo(ReportarActoRequest acto) {

		logger.info("Entrando a insertarActo con parametros: " + acto);
		GenericResponse result = ejbNotificaciones.insertarActo(acto);
		logger.info("Saliendo de insertarActo con respuesta: " + result);
		return result;
	}


}
