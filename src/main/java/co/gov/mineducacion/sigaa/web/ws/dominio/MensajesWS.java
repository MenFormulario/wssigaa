/**
 * 
 */
package co.gov.mineducacion.sigaa.web.ws.dominio;

/**
 * @author rlozada
 *
 */
public interface MensajesWS {
	
	// Respuesta correcta
	public String CODIGO_OK = "0";
	public String MENSAJE_OK = "OK";
	
	// Error credenciales
	public String COD_ERROR_CREDENCIALES = "-1";
	public String MSJ_ERROR_CREDENCIALES = "Sistema no autorizado para consumo del Webservice, contacte al administrador de SIGAA";
	
	// Error Registros vacios
	public String COD_ERROR_REG_VACIOS = "-2";
	public String MSJ_ERROR_REG_VACIOS = "Los registros de Actos Administrativos no son procesables o están vacíos";
	
	// Error registros con error
	public String COD_ERROR_REG_ERRADOS = "-3";
	public String MSJ_ERROR_REG_ERRADOS = "Los registros de Actos Administrativos contienen errores";
	
	// Error Identificador interno de AA en SIGAA es obligatorio
	public String COD_ERROR_ID_OBLIGATORIO = "-4";
	public String MSJ_ERROR_ID_OBLIGATORIO = "El identificador interno en SIGAA y el número de radicado son obligatorios en esta operación";
	
	// Error cargando archivo
	public String COD_ERROR_LOAD_FILE = "-5";
	public String MSJ_ERROR_LOAD_FILE = "Se presentaron errores durante la carga del archivo";
	
	// Error identificador de documento en sistema externo nulo
	public String COD_ERROR_IDOC = "-6";
	public String MSJ_ERROR_IDOC = "Identificador del Acto Administrativo desde el sitema externo no puede ser nulo";
	
	// Error portafirma
	public String COD_ERROR_PORTAFIRMA = "-7";
	public String MSJ_ERROR_PF_CREDENCIALES = "Las Credenciales de portafirma son Obligatorias";
	public String MSJ_ERROR_PF_ASUNTO = "El asunto de portafirma es Obligatorio";
	public String MSJ_ERROR_PF_IDORGGROUP = "El IdOrgGroup de portafirma es Obligatorio";
	public String MSJ_ERROR_PF_NOTICEEND = "El NoticeEnd de portafirma es Obligatorio";
	public String MSJ_ERROR_PF_DOC_NOMBRE = "El nombre del documento de portafirma es Obligatorio";
	public String MSJ_ERROR_PF_NAMEFOLDER = "El NameFolder de portafirma es Obligatorio";
	public String MSJ_ERROR_PF_PARTICIPANTES = "Los Participantes de portafirma son Obligatorios";
	public String MSJ_ERROR_PF_PAR_ADDDOCS = "El AddDocs de portafirma es Obligatorio";
	public String MSJ_ERROR_PF_PAR_ADDSIGNERS = "El AddSigners de portafirma es Obligatorio";
	public String MSJ_ERROR_PF_PAR_CANCEL = "El Cancel de portafirma es Obligatorio";
	public String MSJ_ERROR_PF_PAR_DELETESIGNER = "El DeleteSigner de portafirma es Obligatorio";
	public String MSJ_ERROR_PF_PAR_DIASLIMITE = "El DiasLimite de portafirma es Obligatorio";
	public String MSJ_ERROR_PF_PAR_EDITSIGNER = "El EditSigner de portafirma es Obligatorio";
	public String MSJ_ERROR_PF_PAR_IDACTION = "El IdAction de portafirma es Obligatorio";
	public String MSJ_ERROR_PF_PAR_NIF = "El Nif de portafirma es Obligatorio";
	public String MSJ_ERROR_PF_PAR_NOTICEDAYS = "El NoticeDays de portafirma es Obligatorio";
	public String MSJ_ERROR_PF_PAR_REASSIGNTASK = "El ReassignTask de portafirma es Obligatorio";
	public String MSJ_ERROR_PF_PAR_SIGNDESATENDIDA = "El Signdesatendida de portafirma es Obligatorio";
	public String MSJ_ERROR_PF_PAR_TIMESTAMP = "El Timestamp de portafirma es Obligatorio";
	public String MSJ_ERROR_PF_NIF = "El NIF de portafirma es Obligatorio";
	
	// Error SGD
	public String COD_ERROR_SGD = "-8";
	public String MSJ_ERROR_SGD_HEADER = "La cabecera de radicacion en SGD no puede ser nula";
	public String MSJ_ERROR_SGD_HD_IDAPPENTIDAD = "El IdAppEntidad de la cabecera de radicacion en SGD no puede ser nulo";
	public String MSJ_ERROR_SGD_HD_IDUSUARIO = "El IdUsuario de la cabecera de radicacion en SGD no puede ser nulo";
	public String MSJ_ERROR_SGD_HD_PASSWORD = "El Password de la cabecera de radicacion en SGD no puede ser nulo";
	public String MSJ_ERROR_SGD_RADICADO = "La informacion de radicacion de SGD no puede ser nula";
	public String MSJ_ERROR_SGD_RD_IDUSUARIO = "El IdUsuario es necesario en la informacion de radicacion de SGD";
	public String MSJ_ERROR_SGD_RD_CLAVE = "La Clave es necesario en la informacion de radicacion de SGD";
	public String MSJ_ERROR_SGD_RD_ORIGENNIVEL =  "El OrigenNivel es necesario en la informacion de radicacion de SGD";
	public String MSJ_ERROR_SGD_RD_CTMSORIGENIDNIVEL = "El cTmsOrigenIdNivel es necesario en la informacion de radicacion de SGD";
	public String MSJ_ERROR_SGD_RD_CTMSCLASE = "El cTmsClase es necesario en la informacion de radicacion de SGD";
	public String MSJ_ERROR_SGD_RD_CTMSORIGENCARGO = "El cTmsOrigenCargo es necesario en la informacion de radicacion de SGD";
	public String MSJ_ERROR_SGD_RD_CTMSORIGENTSOL = "El cTmsOrigenTSol es necesario en la informacion de radicacion de SGD";
	public String MSJ_ERROR_SGD_RD_CTMSORIGENUSUARIO = "El cTmsOrigenUsuario es necesario en la informacion de radicacion de SGD";
	public String MSJ_ERROR_SGD_RD_CTMSORIGENIDCARGO = "El cTmsOrigenIdCargo es necesario en la informacion de radicacion de SGD";
	public String MSJ_ERROR_SGD_RD_CTMSORIGENIDTSOL = "El cTmsOrigenIdTSol es necesario en la informacion de radicacion de SGD";
	public String MSJ_ERROR_SGD_RD_TIPODOCUMENTAL = "El TipoDocumental es necesario en la informacion de radicacion de SGD";
	public String MSJ_ERROR_SGD_RD_CANALSALIDA = "El CanalSalida es necesario en la informacion de radicacion de SGD";
	public String MSJ_ERROR_SGD_RD_IDORIGEN = "El IdOrigen es necesario en la informacion de radicacion de SGD";
	public String MSJ_ERROR_SGD_RD_NOMBREREGISTRO = "El NombreRegistro es necesario en la informacion de radicacion de SGD";
	public String MSJ_ERROR_SGD_RD_IDXSLGC = "El IdXslGC es necesario en la informacion de radicacion de SGD";
	public String MSJ_ERROR_SGD_RD_TIPODOCUMENTO = "El TipoDocumento es necesario en la informacion de radicacion de SGD";
	public String MSJ_ERROR_SGD_RD_NODEFOLIOS = "El NodeFolios es necesario en la informacion de radicacion de SGD";
	public String MSJ_ERROR_SGD_RD_ASUNTO = "El Asunto es necesario en la informacion de radicacion de SGD";
	public String MSJ_ERROR_SGD_RD_RESUMEN = "El Resumen es necesario en la informacion de radicacion de SGD";
	public String MSJ_ERROR_SGD_ANEXO = "La informacion del Anexo es requerida en SGD";
	public String MSJ_ERROR_SGD_ANX_DESCRIPCIONARCHIVO = "La DescripcionArchivo es requerida en la informacion del Anexo de SGD";
	public String MSJ_ERROR_SGD_ANX_TIPOANEXO = "El TipoAnexo es requerida en la informacion del Anexo de SGD";
	
	// Error no controlado
	public String COD_ERROR_NO_CONTROLADO = "-100";
	public String MSJ_ERROR_NO_CONTROLADO = "Error inesperado en el aplicativo, se debe validar en el log id -> ";
	

}
