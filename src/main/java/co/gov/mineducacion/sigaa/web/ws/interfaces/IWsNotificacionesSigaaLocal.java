/**
 * 
 */
package co.gov.mineducacion.sigaa.web.ws.interfaces;

import javax.ejb.Local;
import javax.jws.WebService;

import co.gov.mineducacion.sigaa.web.ws.dominio.GenericResponse;
import co.gov.mineducacion.sigaa.web.ws.dominio.ReportarActoRequest;

/**
 * @author rlozada
 *
 */
@Local
@WebService
public interface IWsNotificacionesSigaaLocal {
	public GenericResponse reportarActo(ReportarActoRequest acto);
}
