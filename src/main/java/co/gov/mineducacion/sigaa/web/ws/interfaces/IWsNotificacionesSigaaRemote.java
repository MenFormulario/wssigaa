/**
 * 
 */
package co.gov.mineducacion.sigaa.web.ws.interfaces;

import javax.ejb.Remote;
import javax.jws.WebService;

import co.gov.mineducacion.sigaa.web.ws.dominio.GenericResponse;
import co.gov.mineducacion.sigaa.web.ws.dominio.ReportarActoRequest;

/**
 * @author rlozada
 *
 */
@Remote
@WebService
public interface IWsNotificacionesSigaaRemote {
	public GenericResponse reportarActo(ReportarActoRequest acto);
}
